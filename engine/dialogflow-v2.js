require(Modules.CallList); // Enable CallList module
require(Modules.AI);

  let call;
  let first_name;
  let last_name;
  let phone_number;
  
  let playbackCounter = 0;

// AppEvents.Started dispatched for each CSV record
VoxEngine.addEventListener(AppEvents.Started, function (e) {
  let data = VoxEngine.customData(); // <-- data from CSV string in JSON format
  data = JSON.parse(data);
  first_name = data.first_name;
  last_name = data.last_name;
  phone_number = data.phone_number;
 
  Logger.write(`Calling ${first_name} ${last_name} on ${phone_number}`);
  // Make a call
  call = VoxEngine.callUser(phone_number);
 
  // Trying to detect voicemail
  call.addEventListener(CallEvents.AudioStarted, function(){AI.detectVoicemail(call)});
  // Add event listeners
  call.addEventListener(CallEvents.Connected, handleCallConnected);
  call.addEventListener(CallEvents.Failed, handleCallFailed);
  call.addEventListener(CallEvents.Disconnected, handleCallDisconnected);
  call.addEventListener(AI.Events.VoicemailDetected, voicemailDetected);
});

function voicemailDetected(e) {
  // Voicemail?
  if (e.confidence >= 75) {
    VoxEngine.CallList.reportError('Voicemail', VoxEngine.terminate);
  }
}

// Call connected successfully
function handleCallConnected(e) {
  connected = true;
  setTimeout(function () {
   e.call.say(`Hello ${first_name}! Thank you for visiting our store, ` +
      `please rate the customer service quality from 1 to 5.`, 
      Language.US_ENGLISH_FEMALE);
  }, 500);
  e.call.addEventListener(CallEvents.PlaybackFinished, handlePlaybackFinished);
  e.call.addEventListener(CallEvents.ToneReceived, handleToneReceived);
}

let rating;

function handleToneReceived(e) {
  e.call.removeEventListener(CallEvents.PlaybackFinished, handlePlaybackFinished);
  e.call.stopPlayback();
  rating = e.tone;
  e.call.say('Thank you for your answer!', Language.US_ENGLISH_FEMALE);
  e.call.addEventListener(CallEvents.PlaybackFinished, function (e) {
    e.call.hangup();
  });
}

function handleCallDisconnected(e) {
  // Tell CallList processor about successful call result
  CallList.reportResult({
    result: true,
    duration: e.duration,
    rating: rating,
  }, VoxEngine.terminate);
}

// Playback finished
function handlePlaybackFinished(e) {
  e.call.removeEventListener(CallEvents.PlaybackFinished, handlePlaybackFinished);
  playbackCounter++;
  if (playbackCounter === 3) {
    e.call.hangup();
  } else {
    setTimeout(function () {
      e.call.say('Please rate the customer service quality from 1 to 5.', 
        Language.US_ENGLISH_FEMALE);
      e.call.addEventListener(CallEvents.PlaybackFinished, handlePlaybackFinished);
    }, 2000);
  }
}

function handleCallFailed(e) {
  // Tell CallList processor that we couldn't get call connected
  // depending on the request options it will either try to launch the scenario again after some time
  // or will write the result (failed call) into result_data column of the CSV file with results
  CallList.reportError({
    result: false,
    msg: 'Failed',
    code: e.code
  }, VoxEngine.terminate);
}