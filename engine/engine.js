require(Modules.AI);

var dialogflow, call;

function sendMediaToDialogflow() {
  call.removeEventListener(CallEvents.PlaybackFinished)
  call.sendMediaTo(dialogflow)
}

function onDialogflowResponse(e) {
  let response = e.response;
  if (response.queryResult !== undefined && response.queryResult.fulfillmentText !== undefined) {
  	call.say(response.queryResult.fulfillmentText, Language.Premium.US_ENGLISH_FEMALE)
    call.addEventListener(CallEvents.PlaybackFinished, sendMediaToDialogflow)
  }
}

function onCallConnected(e) {
  Logger.write("call is connected");
  dialogflow = AI.createDialogflow({ lang: DialogflowLanguage.ENGLISH_US })
  dialogflow.addEventListener(AI.Events.DialogflowResponse, onDialogflowResponse) 
  call.say("Здравствуйте! Вас приветствует служба поддержки! Можно задать вам несколько вопросов?", Language.Premium.US_ENGLISH_FEMALE) 
  call.addEventListener(CallEvents.PlaybackFinished, sendMediaToDialogflow)
}

VoxEngine.addEventListener(AppEvents.Started, (e) => {
  call = VoxEngine.callUser("callee", "caller", "VoxImplant Cloud");
  call.addEventListener(CallEvents.Connected, onCallConnected)
  call.addEventListener(CallEvents.Disconnected, VoxEngine.terminate)
  call.answer()
});