<?php
// Send Query
$data = array(
	"contexts" => "YesorNo-followup",
	"query" => "No!",
	"lang" => "en",
    "sessionId" => "12345",
    "timezone" => "Europe/Moscow"
);

$options = array(
	'http' => array(
		'method'  => 'GET',
		'header'=>  "Content-Type: application/json\r\n" .
					"Accept: application/json\r\n".
					"Authorization: Bearer 50dae1a4cbb3461fb25fc9fbf500e045\r\n"
	)
);

$url = "https://api.dialogflow.com/v1/query?v=20150910" .
	"&contexts=YesorNo-followup".
	"&query=" . urlencode("Не не нужно").
	"&lang=en" .
	"&sessionId=" .  floor(100000 * rand()) .
	"&timezone=Europe/Moscow";


$context  = stream_context_create( $options );
$result = file_get_contents( $url, false, $context );
$response = json_decode( $result );

echo "<pre>";
print_r($result);
echo "</pre>";

?>